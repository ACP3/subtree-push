<?php
namespace ACP3\Subtree;

use Symfony\Component\Console\Command\Command;

abstract class AbstractSubtreeCommand extends Command
{
    /**
     * @throws \Exception
     */
    protected function checkoutRepositoriesDir()
    {
        if (!is_dir(Repositories::getRepoDir())) {
            throw new \Exception('Repositories directory doesn\'t exist. Please run acp3:subtree-init command first!');
        }
    }
}
