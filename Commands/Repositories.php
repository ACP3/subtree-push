<?php
namespace ACP3\Subtree;

class Repositories
{
    /**
     * @var array
     */
    protected static $main = [
        'directory' => 'cms',
        'url' => 'https://github.com/ACP3/cms.git'
    ];

    /**
     * @var array
     */
    protected static $modules = [
        'ACP3/Modules/ACP3/Acp/' => [
            'directory' => 'module-acp',
            'url' => 'https://github.com/ACP3/module-acp.git'
        ],
        'ACP3/Modules/ACP3/Articles/' => [
            'directory' => 'module-articles',
            'url' => 'https://github.com/ACP3/module-articles.git'
        ],
        'ACP3/Modules/ACP3/Captcha/' => [
            'directory' => 'module-captcha',
            'url' => 'https://github.com/ACP3/module-captcha.git'
        ],
        'ACP3/Modules/ACP3/Categories/' => [
            'directory' => 'module-categories',
            'url' => 'https://github.com/ACP3/module-categories.git'
        ],
        'ACP3/Modules/ACP3/Comments/' => [
            'directory' => 'module-comments',
            'url' => 'https://github.com/ACP3/module-comments.git'
        ],
        'ACP3/Modules/ACP3/Contact/' => [
            'directory' => 'module-contact',
            'url' => 'https://github.com/ACP3/module-contact.git'
        ],
        'ACP3/Modules/ACP3/Emoticons/' => [
            'directory' => 'module-emoticons',
            'url' => 'https://github.com/ACP3/module-emoticons.git'
        ],
        'ACP3/Modules/ACP3/Errors/' => [
            'directory' => 'module-errors',
            'url' => 'https://github.com/ACP3/module-errors.git'
        ],
        'ACP3/Modules/ACP3/Feeds/' => [
            'directory' => 'module-feeds',
            'url' => 'https://github.com/ACP3/module-feeds.git'
        ],
        'ACP3/Modules/ACP3/Filemanager/' => [
            'directory' => 'module-filemanager',
            'url' => 'https://github.com/ACP3/module-filemanager.git'
        ],
        'ACP3/Modules/ACP3/Files/' => [
            'directory' => 'module-files',
            'url' => 'https://github.com/ACP3/module-files.git'
        ],
        'ACP3/Modules/ACP3/Gallery/' => [
            'directory' => 'module-gallery',
            'url' => 'https://github.com/ACP3/module-gallery.git'
        ],
        'ACP3/Modules/ACP3/Guestbook/' => [
            'directory' => 'module-guestbook',
            'url' => 'https://github.com/ACP3/module-guestbook.git'
        ],
        'ACP3/Modules/ACP3/Menus/' => [
            'directory' => 'module-menus',
            'url' => 'https://github.com/ACP3/module-menus.git'
        ],
        'ACP3/Modules/ACP3/News/' => [
            'directory' => 'module-news',
            'url' => 'https://github.com/ACP3/module-news.git'
        ],
        'ACP3/Modules/ACP3/Newsletter/' => [
            'directory' => 'module-newsletter',
            'url' => 'https://github.com/ACP3/module-newsletter.git'
        ],
        'ACP3/Modules/ACP3/Permissions/' => [
            'directory' => 'module-permissions',
            'url' => 'https://github.com/ACP3/module-permissions.git'
        ],
        'ACP3/Modules/ACP3/Polls/' => [
            'directory' => 'module-polls',
            'url' => 'https://github.com/ACP3/module-polls.git'
        ],
        'ACP3/Modules/ACP3/Search/' => [
            'directory' => 'module-search',
            'url' => 'https://github.com/ACP3/module-search.git'
        ],
        'ACP3/Modules/ACP3/Seo/' => [
            'directory' => 'module-seo',
            'url' => 'https://github.com/ACP3/module-seo.git'
        ],
        'ACP3/Modules/ACP3/System/' => [
            'directory' => 'module-system',
            'url' => 'https://github.com/ACP3/module-system.git'
        ],
        'ACP3/Modules/ACP3/Users/' => [
            'directory' => 'module-users',
            'url' => 'https://github.com/ACP3/module-users.git'
        ],
        'ACP3/Modules/ACP3/Wysiwygckeditor/' => [
            'directory' => 'module-wysiwyg-ckeditor',
            'url' => 'https://github.com/ACP3/module-wysiwyg-ckeditor.git'
        ],
        'ACP3/Modules/ACP3/Wysiwygtinymce/' => [
            'directory' => 'module-wysiwyg-tinymce',
            'url' => 'https://github.com/ACP3/module-wysiwyg-tinymce.git'
        ],
        'ACP3/Core/' => [
            'directory' => 'core',
            'url' => 'https://github.com/ACP3/core.git'
        ],
        'installation/' => [
            'directory' => 'setup',
            'url' => 'https://github.com/ACP3/setup.git'
        ],
        'tests/' => [
            'directory' => 'tests',
            'url' => 'https://github.com/ACP3/test.git'
        ],
        "designs/acp3/" => [
            'directory' => 'theme-default',
            'url' => 'https://github.com/ACP3/theme-default.git'
        ]
    ];

    /**
     * @return array
     */
    public static function getModules()
    {
        return self::$modules;
    }

    /**
     * @return array
     */
    public static function getMain()
    {
        return self::$main;
    }

    /**
     * @return string
     */
    public static function getRepoDir()
    {
        return realpath(__DIR__ . '/../') . '/repositories/';
    }
}
