<?php
/**
 * Created by PhpStorm.
 * User: tinog
 * Date: 29.07.2016
 * Time: 21:45
 */

namespace ACP3\Subtree;


use ACP3\Subtree\Thread\DeleteTagThread;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SubtreeDeleteTagCommand extends AbstractSubtreeCommand
{
    protected function configure()
    {
        $this
            ->setName('acp3:subtree:tag-delete')
            ->setDescription('Deletes a tag of the various ACP3 module repositories.')
            ->addArgument('version', InputArgument::REQUIRED, 'The to be deleted tag.');
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->checkoutRepositoriesDir();

        $version = $input->getArgument('version');

        $repositories = Repositories::getModules();
        $repositories['main'] = Repositories::getMain();

        /** @var DeleteTagThread[] $workers */
        $workers = [];
        $i = 0;
        foreach ($repositories as $prefix => $info) {
            $workers[$i] = new DeleteTagThread($info, $version);
            $workers[$i]->start();
            ++$i;
        }

        foreach (range(0, $i - 1) as $worker) {
            $workers[$worker]->join();
        }

        return 0;
    }
}
