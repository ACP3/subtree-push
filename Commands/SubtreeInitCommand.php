<?php
namespace ACP3\Subtree;

use ACP3\Subtree\Thread\InitializeRepositoryThread;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SubtreeInitCommand extends AbstractSubtreeCommand
{
    protected function configure()
    {
        $this
            ->setName('acp3:subtree:init')
            ->setDescription('Initializes all ACP3 module repositories.');
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->checkoutRepositoriesDir();
        } catch (\Exception $e) {
            mkdir(Repositories::getRepoDir());
        }

        $repositories = Repositories::getModules();
        $repositories['main'] = Repositories::getMain();

        /** @var InitializeRepositoryThread[] $workers */
        $workers = [];
        $i = 0;
        foreach ($repositories as $prefix => $info) {
            $workers[$i] = new InitializeRepositoryThread($info);
            $workers[$i]->start();
            ++$i;
        }

        foreach (range(0, $i - 1) as $worker) {
            $workers[$worker]->join();
        }

        return 0;
    }
}
