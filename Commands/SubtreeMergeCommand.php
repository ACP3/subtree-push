<?php
namespace ACP3\Subtree;


use ACP3\Subtree\Thread\MergeChangesThread;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SubtreeMergeCommand extends AbstractSubtreeCommand
{
    protected function configure()
    {
        $this
            ->setName('acp3:subtree:merge')
            ->setDescription('Merges current changes from the main ACP3 repository into the various ACP3 module repositories.');
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->checkoutRepositoriesDir();
        $this->ensureMasterBranchForMainRepo();

        $currentTime = time();

        /** @var MergeChangesThread[] $workers */
        $workers = [];
        $i = 0;
        foreach (Repositories::getModules() as $prefix => $repository) {
            $workers[$i] = new MergeChangesThread($repository, $prefix, $currentTime);
            $workers[$i]->run();
            ++$i;
        }

//        foreach (range(0, $i - 1) as $worker) {
//            $workers[$worker]->join();
//        }

        return 0;
    }

    private function ensureMasterBranchForMainRepo()
    {
        $repo = Repositories::getMain();
        $path = Repositories::getRepoDir() . $repo['directory'];

        if (is_dir($path)) {
            chdir($path);

            exec('git checkout master');
        }
    }
}
