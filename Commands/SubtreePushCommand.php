<?php
namespace ACP3\Subtree;

use ACP3\Subtree\Thread\PushRepositoryThread;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SubtreePushCommand extends AbstractSubtreeCommand
{
    protected function configure()
    {
        $this
            ->setName('acp3:subtree:push')
            ->setDescription('Pushes all changes to the various ACP3 module repositories.');
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->checkoutRepositoriesDir();

        /** @var PushRepositoryThread[] $workers */
        $workers = [];
        $i = 0;
        foreach (Repositories::getModules() as $prefix => $info) {
            $workers[$i] = new PushRepositoryThread($info);
            $workers[$i]->start();
            ++$i;
        }

        foreach (range(0, $i - 1) as $worker) {
            $workers[$worker]->join();
        }

        return 0;
    }
}
