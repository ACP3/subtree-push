<?php
namespace ACP3\Subtree;

use ACP3\Subtree\Thread\TagVersionThread;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SubtreeTagVersionCommand extends AbstractSubtreeCommand
{
    protected function configure()
    {
        $this
            ->setName('acp3:subtree:tag-version')
            ->setDescription('Adds a new tag to the various ACP3 module repositories.')
            ->addArgument('version', InputArgument::REQUIRED, 'The to be tagged version number.')
            ->addArgument('message', InputArgument::OPTIONAL, 'The message for the version tag.');
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->checkoutRepositoriesDir();

        $version = $input->getArgument('version');
        $message = $input->getArgument('message');

        /** @var TagVersionThread[] $workers */
        $workers = [];
        $i = 0;
        foreach (Repositories::getModules() as $prefix => $info) {
            $workers[$i] = new TagVersionThread($info, $version, $message);
            $workers[$i]->start();
            ++$i;
        }

        foreach (range(0, $i - 1) as $worker) {
            $workers[$worker]->join();
        }


        return 0;
    }
}
