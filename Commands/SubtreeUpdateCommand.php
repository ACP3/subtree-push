<?php
namespace ACP3\Subtree;

use ACP3\Subtree\Thread\UpdateRepositoryThread;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SubtreeUpdateCommand extends AbstractSubtreeCommand
{
    protected function configure()
    {
        $this
            ->setName('acp3:subtree:update')
            ->setDescription('Updates all ACP3 module repositories to the current repository HEAD.');
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->checkoutRepositoriesDir();

        $repositories = Repositories::getModules();
        $repositories['main'] = Repositories::getMain();

        /** @var UpdateRepositoryThread[] $workers */
        $workers = [];
        $i = 0;
        foreach ($repositories as $prefix => $info) {
            $workers[$i] = new UpdateRepositoryThread($info);
            $workers[$i]->start();
            ++$i;
        }

        foreach (range(0, $i - 1) as $worker) {
            $workers[$worker]->join();
        }

        return 0;
    }
}
