<?php
namespace ACP3\Subtree\Thread;


use ACP3\Subtree\Repositories;

class DeleteTagThread extends \Thread
{
    /**
     * @var array
     */
    private $repository;
    /**
     * @var string
     */
    private $taggedVersion;

    /**
     * TagVersionThread constructor.
     * @param array $repository
     * @param string $taggedVersion
     */
    public function __construct(array $repository, $taggedVersion)
    {
        $this->repository = $repository;
        $this->taggedVersion = $taggedVersion;
    }

    public function run()
    {
        $path = Repositories::getRepoDir() . $this->repository['directory'];
        if (is_dir($path)) {
            chdir($path);
            print('Deleting tag ' . $this->taggedVersion . ' for repository: ' . $this->repository['url'] . "\n");

            if ($this->repository['directory'] === 'cms') {
                exec('git checkout master -q');
            }
            exec("git tag -d {$this->taggedVersion}");
            exec("git push origin :refs/tags/{$this->taggedVersion}");
        }
    }
}
