<?php
namespace ACP3\Subtree\Thread;


use ACP3\Subtree\Repositories;

class MergeChangesThread extends \Thread
{
    /**
     * @var array
     */
    private $repository;
    /**
     * @var string
     */
    private $prefix;
    /**
     * @var int
     */
    private $currentTime;

    /**
     * MergeChangesThread constructor.
     * @param array $repository
     * @param string $prefix
     * @param int $currentTime
     */
    public function __construct(array $repository, $prefix, $currentTime)
    {
        $this->repository = $repository;
        $this->prefix = $prefix;
        $this->currentTime = $currentTime;
    }

    public function run()
    {
        $this->handleSplitRepository();
    }

    private function handleSplitRepository()
    {
        $path = Repositories::getRepoDir() . $this->repository['directory'];

        if (is_dir($path)) {
            chdir($path);

            print('Merging module: ' . $this->repository['url'] . "\n");

            exec("git checkout -q -b {$this->getBranchName()}"); // Create and checkout branch
            exec('git rm -q -r .'); // Delete all files in branch

            $this->copyDirectory();

            exec("git add ."); // Add the files

            exec("git commit -q -am \"Sync module {$this->repository['directory']} with the main repository.\"");

            exec("git checkout -q master");
            exec("git merge -q {$this->getBranchName()}");
            exec("git branch -q -d {$this->getBranchName()}");
        }
    }

    /**
     * @return string
     */
    private function getBranchName()
    {
        return 'develop-' . $this->currentTime;
    }

    private function copyDirectory()
    {
        $repoDir = Repositories::getRepoDir();
        $mainRepoRootDir = $repoDir . Repositories::getMain()['directory'];

        // "." is required to copy hidden files too
        exec("cp -r {$mainRepoRootDir}/{$this->prefix}. {$repoDir}{$this->repository['directory']}");
    }
}
