<?php
namespace ACP3\Subtree\Thread;


use ACP3\Subtree\Repositories;

class PushRepositoryThread extends \Thread
{
    /**
     * @var array
     */
    private $repository;

    /**
     * PushRepositoryThread constructor.
     * @param array $repository
     */
    public function __construct(array $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        $path = Repositories::getRepoDir() . $this->repository['directory'];
        if (is_dir($path)) {
            chdir($path);
            print('Pushing module: ' . $this->repository['url'] . "\n");
            exec('git push -q');
            exec('git push --tags -q');
        }
    }
}
