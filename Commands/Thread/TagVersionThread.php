<?php
namespace ACP3\Subtree\Thread;


use ACP3\Subtree\Repositories;

class TagVersionThread extends \Thread
{
    /**
     * @var array
     */
    private $repository;
    /**
     * @var string
     */
    private $taggedVersion;
    /**
     * @var string
     */
    private $tagMessage;

    /**
     * TagVersionThread constructor.
     * @param array $repository
     * @param string $taggedVersion
     * @param string $tagMessage
     */
    public function __construct(array $repository, $taggedVersion, $tagMessage)
    {
        $this->repository = $repository;
        $this->taggedVersion = $taggedVersion;
        $this->tagMessage = $tagMessage;
    }

    public function run()
    {
        $path = Repositories::getRepoDir() . $this->repository['directory'];
        if (is_dir($path)) {
            chdir($path);
            print('Tagging version ' . $this->taggedVersion . ' for repository: ' . $this->repository['url'] . "\n");

            exec("git tag {$this->taggedVersion} -am \"{$this->getTagMessage()}\"");
        }
    }

    /**
     * @return string
     */
    private function getTagMessage()
    {
        return (empty($this->tagMessage)) ? $this->tagMessage : "Tagging version {$this->taggedVersion}.";
    }
}
