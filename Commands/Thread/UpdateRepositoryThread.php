<?php
namespace ACP3\Subtree\Thread;


use ACP3\Subtree\Repositories;

class UpdateRepositoryThread extends \Thread
{
    /**
     * @var array
     */
    private $repository;

    /**
     * UpdateRepositoryThread constructor.
     * @param array $repository
     */
    public function __construct(array $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        if ($this->repository['directory'] === 'cms') {
            $this->handleMainRepository();
        } else {
            $this->handleSplitRepository();
        }
    }

    private function handleMainRepository()
    {
        $path = Repositories::getRepoDir() . $this->repository['directory'];
        if (is_dir($path)) {
            chdir($path);
            print('Updating: ' . $this->repository['url'] . "\n");

            // Fetch and pull all remote branches
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                exec('for /F %r in (\'git branch -r\') do ( git branch --track %r) && git fetch --all && git pull --all');
            } else {
                exec('git branch -r | grep -v \'\->\' | while read remote; do git branch --track "${remote#origin/}" "$remote"; done');
            }
            exec('git fetch --all');
            exec('git pull --all');
        }
    }

    private function handleSplitRepository()
    {
        $path = Repositories::getRepoDir() . $this->repository['directory'];
        if (is_dir($path)) {
            chdir($path);
            print('Updating: ' . $this->repository['url'] . "\n");
            exec("git pull -q");
        }
    }
}
