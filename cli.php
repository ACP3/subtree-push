#!/usr/bin/env php
<?php

if (is_file(__DIR__ . '/../../autoload.php')) {
    require_once __DIR__ . '/../../autoload.php';
} elseif (is_file(__DIR__ . '/vendor/autoload.php')) {
    require_once __DIR__ . '/vendor/autoload.php';
} else {
    throw new \Exception('Composer Autoloader could not be found!');
}

use Symfony\Component\Console\Application;

error_reporting(E_ALL);

$application = new Application();
$application->add(new \ACP3\Subtree\SubtreeInitCommand());
$application->add(new \ACP3\Subtree\SubtreeUpdateCommand());
$application->add(new \ACP3\Subtree\SubtreeMergeCommand());
$application->add(new \ACP3\Subtree\SubtreePushCommand());
$application->add(new \ACP3\Subtree\SubtreeTagVersionCommand());
$application->add(new \ACP3\Subtree\SubtreeDeleteTagCommand());
$application->run();
