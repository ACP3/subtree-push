#!/bin/bash

BASEDIR=$(dirname "$0")

php ${BASEDIR}/cli.php acp3:subtree:init
php ${BASEDIR}/cli.php acp3:subtree:update
php ${BASEDIR}/cli.php acp3:subtree:merge

if [ -z "$1" ]
then
    echo "Skipped version tagging"
else
    php ${BASEDIR}/cli.php acp3:subtree:tag-version $1 "$2"
fi

php ${BASEDIR}/cli.php acp3:subtree:push
